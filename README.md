# Spectrogram

A class for offline spectral processing. Load audio, generate time-frequency representation of the Weyl-Heisenberg variety, process in time-frequency domain and then return to audio domain using Griffin-Lim algorithm. Additionally, an image may be created allowing the user to view the time-frequency representation. 

Contained in this project are the ```Spectrogram``` and ```Window``` objects, as well as a few audio/image read/write functions in ```ezio.h```.

In its most basic form there is no processing; audio is simply converted to the time-frequency domain and back. There is no graphic interface for this program. File names are provided as command line arguments. An example file is included, and executing 

```./Spectrogram```

in the terminal will process the example file.

To use with other files, use the following argument order:

```./Spectrogram inputFileName outputFileName ```

where both are relative file paths. 

