/*
  ==============================================================================

    Window.h
    Created: 3 Mar 2020 1:17:03pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Signal.h"
#pragma once

class Window
{
public:
    //=========== Constructors =================================================
    Window(int mSize);
    Window(String mType, int mSize);
    Window(String mType, int mSampleRate, float duration);
    
    //=========== Initialize ===================================================
    void fillWindow();
    
    //=========== Window Types =================================================
    float hann(int n);
    float hamming(int n);
    float blackman(int n);
    float rect(int n);
    
    //=========== Getters ======================================================
    String getType();
    int getSize();
    int getSampleRate();
    float getDuration();
    float getValue(int index);
    
    
    
    //=========== Signal ======================================================
    void zeroPad(int L);
    
    //=========== Helpers ======================================================
    void printWindow();
    
private:
    AudioBuffer<float> signal;
    String type;
    int sampleRate {48000};
    float duration;
    int size;
};
