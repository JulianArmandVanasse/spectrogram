/*
  ==============================================================================

    ezio.h
    Created: 8 Mar 2020 9:22:54pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

AudioBuffer<float> loadAudio(String name)
{
    File file(name);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    AudioFormatReader *reader = formatManager.createReaderFor(file);
    
    AudioBuffer<float> signal(1, reader->lengthInSamples);
    reader->read(&signal, 0, reader->lengthInSamples, 0, true, true);
    
    delete reader;
    
    return signal;
}

void writeAudio(String name, AudioBuffer<float> audio)
{
    File file(name);
    file.deleteFile();
    WavAudioFormat wav;
    std::unique_ptr<AudioFormatWriter> writer;
    writer.reset(wav.createWriterFor(new FileOutputStream(file.getFullPathName()), 48000, 1, 32, {}, 0));
    writer->writeFromAudioSampleBuffer(audio, 0, audio.getNumSamples());
}

void writeImage(String name, Image image)
{
    File file(name);
    file.deleteFile();
    FileOutputStream stream (file);
    PNGImageFormat pngWriter;
    pngWriter.writeImageToStream(image, stream);
}
