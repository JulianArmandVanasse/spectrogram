/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Spectrogram.h"
#include "Window.h"
#include "Signal.h"
#include "ezio.h"

//==============================================================================
int main (int argc, char* argv[])
{
    if (argc == 1)
    {
        argv[1] = "./audio/i_want_you_back.wav";
        argv[2] = "./output.wav";
    }
    
    std::cout << "\n" << "Processing audio";
    std::cout << "\n" << argv[1] << "\n" << argv[2] << "\n";
    
    AudioBuffer<float> audio = loadAudio(argv[1]);
    Spectrogram TF(audio);
    
    // perform some processing in time frequency domain
    
    Image TFImage = TF.magnitude2Image();
    writeImage("./spectrogram.png", TFImage);
    
    AudioBuffer<float> out = TF.invertTransform();
    writeAudio(argv[2], out);
    
    return 0;
}
