/*
  ==============================================================================

    Spectrogram.h
    Created: 3 Mar 2020 1:16:58pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Window.h"
#pragma once

#define D_WIN_SIZE 4096
#define D_HOP_SIZE 512
#define D_FFT_SIZE 4096

#define EPS 0.000000001f;

class Spectrogram
{
public:
    //=========== Constructors =================================================
    Spectrogram(AudioBuffer<float> mSignal);
    Spectrogram(AudioBuffer<float> mSignal, int mWindowSize);
    Spectrogram(AudioBuffer<float> mSignal, Window mWindow);
    Spectrogram(AudioBuffer<float> mSignal, Window mWindow, int mfftSize);
    
    Spectrogram(AudioBuffer<float> Magnitude, AudioBuffer<float> Phase);
    
    //=========== Initializers =================================================
    void initX();
    void initSignal(AudioBuffer<float> mSignal);
    
    //=========== Transformation ===============================================
    void transform();
    AudioBuffer<float> invertTransform();
    AudioBuffer<float> invertAndNormalize();
    
    Array<Array<float>> mag2Array();
    
    //=========== Display =========================================================
    Image magnitude2Image();
    Image magnitude2ImageWithLimits(int mMin, int mMax, int kMin, int kMax);
    
    //============ Operators ===================================================
    Spectrogram sum(Spectrogram X, Spectrogram Y);
    Spectrogram multiply(Spectrogram X, Spectrogram Y);
    
    //=========== Helpers ======================================================
    void print();
    
    //============ Getters =====================================================
    int getNumFreqBins();
    int getNumFrames();
    AudioBuffer<float> getXmagnitude();
    AudioBuffer<float> getXphase();
    
private:
    //=========== Math =========================================================
    void findRange();
    float rescaleRange(float x);
    float mag(float a, float b);
    float ang(float a, float b);
    float lin2dB(float x);
    
    AudioBuffer<float> signal;              // time domain input
    int sampleRate;
    Array<float> t;
    
    AudioBuffer<float> Xmagnitude, Xphase;  // spectrogram
    Array<float> n, f;
    int fftSize {D_FFT_SIZE};
    int hopSize {D_HOP_SIZE};
    int numFreqBins {D_FFT_SIZE/2 + 1};
    int numFrames;
    float maxMagnitude {0.0f}, minMagnitude {0.0f};
    
    Window window {Window(D_WIN_SIZE)};
    int windowSize {D_WIN_SIZE};

};
