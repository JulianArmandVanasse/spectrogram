/*
  ==============================================================================

    Spectrogram.cpp
    Created: 3 Mar 2020 1:16:58pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Spectrogram.h"
#include "Window.h"

Spectrogram::Spectrogram(AudioBuffer<float> mSignal)
{
    // init fftSize
    fftSize = D_FFT_SIZE;
    hopSize = D_HOP_SIZE;
    
    // init window
    window = Window(D_WIN_SIZE);
    
    // init signal
    initSignal(mSignal);
    
    // init Xmagnitude and Xphase
    initX();
    
    // transform signal
    transform();
    
    // get range
    findRange();
}

Spectrogram::Spectrogram(AudioBuffer<float> mSignal, int mWindowSize)
{
    windowSize = mWindowSize;
    window = Window(windowSize);
    
    fftSize = nextPowerOfTwo(mWindowSize);
    std::cout << "winSz = " << windowSize << ", fftsz = " << fftSize << "\n";
}

Spectrogram::Spectrogram(AudioBuffer<float> Magnitude, AudioBuffer<float> Phase)
{
    Xmagnitude = Magnitude;
    Xphase = Phase;
    
    numFreqBins = Xmagnitude.getNumChannels();
    numFrames = Xmagnitude.getNumSamples();
    fftSize = ((numFreqBins-1) * 2);
    windowSize = fftSize;
    hopSize = ceil(fftSize / 4.0f);
    
    findRange();
}

//=========== Initializers =================================================
//
// malloc for Buffer, Xmagnitude and Xphase
//
void Spectrogram::initX()
{
    numFrames = static_cast<int>(ceil(static_cast<float>(signal.getNumSamples() - fftSize) / static_cast<float>(hopSize)));
    numFreqBins = (fftSize / 2) + 1;
    
    Xmagnitude = AudioBuffer<float>(numFreqBins, numFrames);
    Xmagnitude.clear();
    Xphase = Xmagnitude;
}

//
// check signal dimensions and force to mono
//
void Spectrogram::initSignal(AudioBuffer<float> mSignal)
{
    // check dimensions and fold to mono
    int nChannels = mSignal.getNumChannels();
    int nSamples = mSignal.getNumSamples();
    
    // check signal size
    if (nSamples < window.getSize()) jassert("Spectrogram input signal is too small :(");
    
    // fold to mono
    if (nChannels > 1) {
        AudioBuffer<float> dummyBuffer(1, nSamples);
        dummyBuffer.copyFrom(0, 0, mSignal, 0, 0, nSamples);
        mSignal.makeCopyOf(dummyBuffer);
    }
    
    // assign input to attribute
    signal = mSignal;
}

//=========== Transformation ===============================================
//
// divide signal into overlapping buffers by hopSize and fftSize, and apply
// window. Here buffer is a verb :^)
//
void Spectrogram::transform()
{
    // init fft object
    dsp::FFT fft(round(log2(fftSize)));

    // FFT transforms float arrays, so must copy into float
    // maybe use float pointer to channel??
    float frame[fftSize * 2];
    for (int m = 0; m*hopSize < signal.getNumSamples() - fftSize; m++)
    {
        for (int n = 0; n < fftSize; n++)
        {
            frame[n] = signal.getSample(0, m*hopSize + n) * window.getValue(n);
        }
        fft.performRealOnlyForwardTransform(frame, true);

        int idx = 0;
        for (int k = 0; k < fftSize+1; k+=2)
        {
            // car2pol
            float r = mag(frame[k], frame[k+1]);
            float phi = ang(frame[k], frame[k+1]);

            // store transfrom data
            Xmagnitude.setSample(idx, m, r);
            Xphase.setSample(idx, m, phi);
            idx ++;
        }
    }
}

//============ Inverse Transform ===================================================
//
// Overlap and Add synthesis.
// Spectrogram --> audio
//
AudioBuffer<float> Spectrogram::invertTransform()
{
    
    dsp::FFT fft(ceil(log2(fftSize)));
    
    AudioBuffer<float> result(1, numFrames * hopSize + fftSize);
    
    float frame[2*fftSize];
    
    for (int m = 0; m < numFrames; m++)
    {
        int k = 0;
        for (int l = 0; l < fftSize+1; l+=2)
        {
            frame[l+1] = Xmagnitude.getSample(k, m) * cos(Xphase.getSample(k, m));
            frame[l] = Xmagnitude.getSample(k, m) * sin(Xphase.getSample(k, m));
            k++;
        }
        
        fft.performRealOnlyInverseTransform(frame);
        
        for (int i = 0; i < window.getSize(); i++)
        {
            frame[i] = frame[i] * window.getValue(i);
        }
        
        // Overlap and Add
        int idx = 0;
        for (int n = m*hopSize; n < m*hopSize+fftSize; n++)
        {
            float data = result.getSample(0,n);
            result.setSample(0, n, data + frame[idx]);
            idx++;
        }
        
    }
    return result;
}

AudioBuffer<float> Spectrogram::invertAndNormalize()
{
    AudioBuffer<float> result = this->invertTransform();
    
    float max = -9999999999999999.9;
    for (int i = 0; i < result.getNumSamples(); i++)
    {
        if (abs(result.getSample(0, i)) > max) max = abs(result.getSample(0, i));
    }
    
    if (max!=0)
    {
        result.applyGain(0, result.getNumSamples(), 1.0f/max);
    }
    return result;
}

//============ Display =====================================================
//
// Convert magnitude to Image object for visualizing TF data
//
Image Spectrogram::magnitude2Image()
{
    // create image
    Image result(Image::RGB, numFrames, numFreqBins, true);
    
    // copy to local variable
    AudioBuffer<float> magnitude = Xmagnitude;
    
    // loop over and assign valeus
    for (int m = 0; m < numFrames; m++)
    {
        for (int k = 0; k < numFreqBins; k++)
        {
            float element = magnitude.getSample(k, m);
            element = (element - minMagnitude) / (maxMagnitude - minMagnitude);
            uint8 r = floor(element * (255)) - 1;
            uint8 b = 255 - floor(element * 255);
            Colour colour(r, 0, b);
            result.setPixelAt(m, numFreqBins-k, colour);
        }
    }
    
    return result;
}

Image Spectrogram::magnitude2ImageWithLimits(int mMin, int mMax, int kMin, int kMax)
{
    if (mMin > mMax) jassert("mMin must be less than mMax");
    if (kMin > kMax) jassert("kMin must be less than kMax");
    
    // create image
    Image result(Image::RGB, abs(mMax - mMin), abs(kMax - kMin), true);
    
    // copy to local variable
    AudioBuffer<float> magnitude = Xmagnitude;
    
    // loop over and assign valeus
    for (int m = 0; m < numFrames - mMax - mMin; m++)
    {
        for (int k = 0; k < numFreqBins - mMin - mMin; k++)
        {
            float element = magnitude.getSample(k + mMin, m + mMax);
            element = (element - minMagnitude) / (maxMagnitude - minMagnitude);
            uint8 r = floor(element * (255));
            uint8 b = 255 - floor(element * 255);
            Colour colour(r, 0, b);
            result.setPixelAt(m, kMax-k, colour);
        }
    }
    
    return result;
}


//============ Operators ===================================================
Spectrogram Spectrogram::sum(Spectrogram X, Spectrogram Y)
{
    if (X.getNumFrames() != Y.getNumFrames()) jassert("dimension: frame missmatch");
    if (X.getNumFreqBins() != Y.getNumFreqBins()) jassert("dimension: freqBin missmatch");
    
    AudioBuffer<float> Xm = X.getXmagnitude();
    AudioBuffer<float> Xp = X.getXphase();
    AudioBuffer<float> Ym = Y.getXmagnitude();
    AudioBuffer<float> Yp = Y.getXphase();
    
    AudioBuffer<float> Zm(X.getNumFreqBins(), X.getNumFrames());
    Zm.clear();
    AudioBuffer<float> Zp = Zm;
    
    int numChannels = X.getNumFreqBins();
    int numSamples = X.getNumFrames();
    
    for (int m = 0; m < numSamples; m++)
    {
        for (int k = 0; k < numChannels; k++)
        {
            float xr = Xm.getSample(k, m) * cos(Xp.getSample(k, m));
            float xi = Xm.getSample(k, m) * sin(Xp.getSample(k, m));
            float yr = Ym.getSample(k, m) * cos(Yp.getSample(k, m));
            float yi = Ym.getSample(k, m) * sin(Yp.getSample(k, m));
            
            float zr = xr + yr;
            float zi = xi + yi;
            float zm = mag(zr, zi);
            float zp = ang(zr, zi);
            
            Zm.setSample(k, m, zm);
            Zp.setSample(k, m, zp);
        }
        std::cout << "k = " << m << " out of " << numSamples << "\n";
    }
    
    return Spectrogram(Zm, Zp);
}




//=========== Math =========================================================
void Spectrogram::findRange()
{
    maxMagnitude = -9999999999999999.9f;
    minMagnitude = 99999999999999999.9f;
    for (int m = 0; m < numFrames; m++)
    {
        for (int k = 0; k < numFreqBins; k++)
        {
            float currentElement = Xmagnitude.getSample(k, m);
            if (currentElement > maxMagnitude) maxMagnitude = lin2dB(currentElement);
            if (currentElement < minMagnitude) minMagnitude = lin2dB(currentElement);
        }
    }
}

float Spectrogram::rescaleRange(float x)
{
    return (x - minMagnitude) / (maxMagnitude - minMagnitude);
}

float Spectrogram::mag(float a, float b)
{
    return sqrt(pow(a, 2.0f) + pow(b, 2.0f));
}

float Spectrogram::ang(float a, float b)
{
    return atan2(a, b);
}

float Spectrogram::lin2dB(float x)
{
    return 20*log10(x) + EPS;
}


//============ Getters ===================================================
int Spectrogram::getNumFreqBins()
{
    return numFreqBins;
}

int Spectrogram::getNumFrames()
{
    return numFrames;
}

AudioBuffer<float> Spectrogram::getXmagnitude()
{
    return Xmagnitude;
}

AudioBuffer<float> Spectrogram::getXphase()
{
    return Xphase;
}


//=========== Helpers ======================================================
void Spectrogram::print()
{
    std::cout << "Spectrogram, \t signal has " << signal.getNumSamples() << " samples \n";
    std::cout << "Xmagnitude has " << Xmagnitude.getNumChannels() << " channels and ";
    std::cout << Xmagnitude.getNumSamples() << " samples \n";
    
    std::cout << "max and min are " << maxMagnitude << " and " << minMagnitude << "\n";
    
    for (int m = 0; m < numFrames; m++)
    {
        std::cout << "\n";
        for (int k = 0; k < numFreqBins; k++)
        {
            float element = Xmagnitude.getSample(k, m);
            element = rescaleRange(element);
            
            if (element < 0.2)
            {
                std::cout << "   ";
            } else {
                std::cout << " * ";
            }
        }
    }
    std::cout << "\n";
}
