/*
  ==============================================================================

    Window.cpp
    Created: 3 Mar 2020 1:17:03pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Window.h"

#define W_HANN "hann"
#define W_RECT "rect"
#define W_BLACK "blackman"
#define W_HAMM "hamming"

Window::Window(int mSize)
{
    size = mSize;
    signal = AudioBuffer<float>(1, size);
    type = "hann";
    fillWindow();
}

//=================== Initializers =============================================
void Window::fillWindow()
{
    // empty signal buffer
    signal.clear();
    
    // iterate over samples and fill with values
    for (int n = 0; n < size; n++)
    {
        if (!type.compare(W_HANN)) signal.setSample(0, n, hann(n));
        if (!type.compare(W_RECT)) signal.setSample(0, n, 1.0f);
    }
}

float Window::hann(int n)
{
    float fn = static_cast<float>(n);
    float fN = static_cast<float>(size);
    return pow(sin(M_PI * fn / (fN - 1.0)),2.0);
}

//=================== Getters ==================================================
int Window::getSize()
{
    return size;
}

float Window::getValue(int index)
{
    if (index < 0 || index > signal.getNumSamples()) jassert("out of range for this window size");
    return signal.getSample(0, index);
}

//=================== Signal  ==================================================
void Window::zeroPad(int L)
{
    if (size > L) jassert("cannot zeropad to less than length of window");
    AudioBuffer<float> newData(1, L);
    newData.clear();
    newData.copyFrom(0, 0, signal, 0, 0, size);
    signal = AudioBuffer<float>(1, L);
    signal.copyFrom(0, 0, newData, 0, 0, L);
    size = L;
}

//=================== Helpers ==================================================
void Window::printWindow()
{
    std::cout << "window: \n";
    std::cout << "type = " << type << "\n";
    for (int i = 0; i < size; i++)
    {
        std::cout << "window[" << i << "] = \t";
        std::cout << signal.getSample(0, i) << "\n";
    }
}
